import copy
import numpy as np
import pandas as pd
import HelperModules.HelperFunctions as hf
from HelperModules.Directories import Directories
from HelperModules.Preprocessing import Preprocess

class DataHandler(object):
    def __init__(self,Settings):
        ###################################################################
        ############################ Constants ############################
        ###################################################################
        self.__WEIGHTNAME     = "Weight"                               # Name of weight column in df
        self.__PENALTYNAME    = "PenaltyFactor"                        # Name of penalty factor column in df
        self.__LABELNAME      = "Label"                                # Name of label column in df
        self.__SAMPLENAME     = "Sample_Name"                          # Name of sample name column in df
        self.__SAMPLETYPE     = "Sample_Type"                          # Name of sample type column in df
        self.__PREDICTIONNAME = "Prediction"                           # Name of prediction column in df
        self.__FOLDIDENTIFIER = "eventNumber"                          # Name of fold identifier column in df
        self.__ISNOMINAL      = "isNominal"                            # Name of the identifier defining the nominal tree

        ###################################################################
        ####################### Settings variables ########################
        ###################################################################
        self.__Samples         = Settings.get_Samples()                 # List of all samples
        self.__NominalSamples  = Settings.get_NominalSamples()          # List of nominal samples
        self.__SystSamples     = Settings.get_SystSamples()             # List of sytematic samples
        self.__OutputPath      = Settings.get_General().get_Job()       # Path to the 'JOB' directory
        self.__nFolds          = Settings.get_General().get_Folds()     # Number of folds
        self.__Variables       = Settings.get_General().get_Variables() # List of Variables
        self.__ModelSettings   = Settings.get_Model()                   # Object holding general Model information
        self.__GeneralSettings = Settings.get_General()                 # Object holding general information
        self.__ClassLabels     = self.__ModelSettings.get_ClassLabels() # List of class labels
        self.__Dirs            = Directories(self.__OutputPath)         # Directory object

        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        try:
            self.__Scaler = load(open(hf.ensure_trailing_slash(self.__Dirs.ModelDir())+"scaler.pkl",'rb'))
        except:
            self.__Scaler = None
        self.__DataFrame = pd.read_hdf(hf.ensure_trailing_slash(self.__Dirs.DataDir())+"merged.h5")
        self.__DataFrame["Index_tmp"] = np.arange(len(self.__DataFrame))
        F_tmp = self.__DataFrame[self.__FOLDIDENTIFIER].values # temporarily save fold identifier to reattach them later
        X_tmp, W_tmp, _  = Preprocess(X=copy.deepcopy(self.__DataFrame[self.__Variables].values),
                                      Y=copy.deepcopy(self.__DataFrame[self.__LABELNAME].values),
                                      W=copy.deepcopy(self.__DataFrame[self.__WEIGHTNAME].values),
                                      settings=Settings,
                                      scaler=self.__Scaler)
        Y_tmp = copy.deepcopy(self.__DataFrame[self.__LABELNAME].values)
        if self.__ISNOMINAL in self.__DataFrame: # for older versions this variable might not exist yet
            isNominal_tmp = copy.deepcopy(self.__DataFrame[self.__ISNOMINAL].values)
        else:
            isNominal_tmp = [True]*len(Y_tmp)
            self.__DataFrame[self.__ISNOMINAL] = isNominal_tmp
        N_tmp = copy.deepcopy(self.__DataFrame[self.__SAMPLENAME].values)
        self.__DataFrame_sc = pd.DataFrame(data=X_tmp, columns = self.__Variables)
        self.__DataFrame_sc[self.__WEIGHTNAME] = W_tmp
        self.__DataFrame_sc[self.__LABELNAME] = Y_tmp
        self.__DataFrame_sc[self.__ISNOMINAL] = isNominal_tmp
        self.__DataFrame_sc[self.__SAMPLENAME] = N_tmp
        self.__DataFrame_sc[self.__FOLDIDENTIFIER] = F_tmp # reattaching fold identifer
        self.__DataFrame_sc["Index_tmp"] = np.arange(len(self.__DataFrame_sc))
        self.apply_Penalty()

    ###################################################################
    ########################## Class Methods ##########################
    ###################################################################
    def apply_Penalty(self):
        Penalty_dict = {Sample.get_Name(): Sample.get_PenaltyFactor() for Sample in self.__Samples}
        N_tmp = self.__DataFrame_sc[self.__SAMPLENAME].values.tolist()
        Penalty_Factor = np.array([Penalty_dict[SampleName] for SampleName in N_tmp])
        self.__DataFrame_sc[self.__PENALTYNAME] = Penalty_Factor
        self.__DataFrame_sc[self.__WEIGHTNAME] = self.__DataFrame_sc[self.__PENALTYNAME]*self.__DataFrame_sc[self.__WEIGHTNAME]

    def get_Labels(self, returnNominalOnly=True):
            return self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]

    def get_TrainInputs_prescaled(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        '''
        Function to return training inputs but before they are scaled.

        Keyword Arguments:
        Fold                --- Number (integer) of the fold to be used
        returnNonZeroLabels --- Whether samples with negative training labels should be taken into account
        returnNominalOnly   --- Whether only nominal samples (no systematics) should be taken into account
        '''
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[self.get_Variables()]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_Variables()]

    def get_TestInputs_prescaled(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        '''
        Function to return testing inputs but before they are scaled.

        Keyword Arguments:
        Fold                --- Number (integer) of the fold to be used
        returnNonZeroLabels --- Whether samples with negative training labels should be taken into account
        returnNominalOnly   --- Whether only nominal samples (no systematics) should be taken into account
        '''
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[self.get_Variables()]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_Variables()]

    def get_TrainInputs(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[self.get_Variables()]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_Variables()]

    def get_TestInputs(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[self.get_Variables()]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__LABELNAME]>=0][self.get_Variables()]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_Variables()]

    def get_TrainLabels(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.__LABELNAME]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[self.__LABELNAME]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]>=0][self.__LABELNAME]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]

    def get_TestLabels(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.__LABELNAME]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[self.__LABELNAME]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]>=0][self.__LABELNAME]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]

    def get_TrainWeights(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[self.__WEIGHTNAME]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.__WEIGHTNAME]

    def get_TestWeights(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[self.__WEIGHTNAME]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.__LABELNAME]

    def get_TrainWeights_sc(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()!=Fold]
            return tmp[self.__WEIGHTNAME]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.__WEIGHTNAME]

    def get_TestWeights_sc(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if returnNonZeroLabels==False and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[tmp[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds!=1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
            return tmp[self.__WEIGHTNAME]
        if returnNonZeroLabels==False and self.__nFolds==1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.__LABELNAME]>=0][self.__WEIGHTNAME]
        if returnNonZeroLabels==True and self.__nFolds==1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.__LABELNAME]

    def get_Weights(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__WEIGHTNAME]

    def get_FoldIdentifier(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]
    
    def get_Sample_Names(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__SAMPLENAME]

    def get_Sample_Types(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__SAMPLETYPE]
    
    def get_DataFrame(self, returnNominalOnly=True):
        if returnNominalOnly==True:
            return self.__DataFrame[self.__DataFrame[self.__ISNOMINAL]==returnNominalOnly]
        else:
            return self.__DataFrame

    def get_DataFrame_sc(self, returnNominalOnly=True):
        if returnNominalOnly==True:
            return self.__DataFrame_sc[self.__DataFrame_sc[self.__ISNOMINAL]==returnNominalOnly]
        else:
            return self.__DataFrame_sc
    
    def get_nFolds(self):
        return self.__nFolds
    
    def get_Variables(self):
        return self.__Variables

    def get_Samples(self):
        return self.__Samples

    def get_NominalSamples(self):
        return self.__NominalSamples

    def get_SystSamples(self):
        return self.__SystSamples

    def get(self, variable, returnNominalOnly=True):
        return self.__DataFrame[self.__DataFrame[self.__ISNOMINAL]==returnNominalOnly][variable]

    def get_sc(self, variable, returnNominalOnly=True):
        return self.__DataFrame_sc[self.__DataFrame_sc[self.__ISNOMINAL]==returnNominalOnly][variable]

    def get_ModelDirectory(self):
        return self.__Dirs.ModelDir()
    
    def get_MVAPlotDirectory(self):
        return self.__Dirs.MVAPlotDir()

    def get_CtrlPlotDirectory(self):
        return self.__Dirs.CtrlPlotDir()

    def get_ModelSettings(self):
        return self.__ModelSettings

    def get_ClassLabels(self):
        return self.__ClassLabels
