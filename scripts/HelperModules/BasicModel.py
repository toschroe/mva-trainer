from HelperModules.MessageHandler import ErrorMessage, WarningMessage
import tensorflow as tf
import HelperModules.HelperFunctions as hf
import keras

class BasicModel(keras.Model):
    """
    This class serves as the base class for any machine learning model in the framework.
    All major properties should be defined in here and then passed down via inheritance.
    All sub-classes should implicitly call the constructor of this class.
    """
    def __init__(self, group):
        self.__Group               = group     # String containing all config options for the model
        self.__Name                = None      # Name of the model
        self.__Type                = None      # Type of the model (e.g. "Classification")
        self.__Epochs              = 100       # Maximum training epochs
        self.__BatchSize           = 32        # Batch size
        self.__ValidationSize      = 0.2       # Size of the validation set (0.2=20%)
        self.__Metrics             = None      # Metrics to be evaluated during training (e.g. accuracy)
        self.__ClassLabels         = None      # Labels for multi-class classification
        self.__ModelBinning        = None      # Chosen binning for NN Output plots (e.g. 10,0,1)
        self.__ClassColors         = None      # Colors for each Class for the stacked plots


        self.__AllowedTypes        = ["Classification-DNN",
                                      "Classification-BDT",
                                      "Z-Reconstruction",
                                      "Classification-WGAN"]

        # Making sure tensorflow only grabs as much memory as it needs and not ALL OF IT!
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                for gpu in gpus:
                    tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        super(BasicModel, self).__init__()

        #############################################################################
        ############################ Basic Input Options ############################
        #############################################################################
        for item in group:
            if "Name" in item:
                self.__Name = hf.ReadOption(item, "Name", istype="str", islist=False)
            if "Type" in item:
                self.__Type = hf.ReadOption(item, "Type", istype="str", islist=False)
            if "Epochs" in item:
                self.__Epochs = hf.ReadOption(item, "Epochs", istype="int", islist=False, msg="Could not load 'Epochs'. Make sure you pass an integer.")
            if "BatchSize" in item:
                self.__BatchSize = hf.ReadOption(item, "BatchSize", istype="int", islist=False, msg="Could not load 'BatchSize'. Make sure you pass an integer.")
            if "ValidationSize" in item:
                self.__ValidationSize = hf.ReadOption(item, "ValidationSize", istype="float", islist=False, msg="Could not load 'ValidationSize'. Make sure you pass a float between 0 and 1.")
            if "Metrics" in item:
                self.__Metrics = hf.ReadOption(item, "Metrics", istype="str", islist=True)
            if "ClassLabels" in item:
                self.__ClassLabels = hf.ReadOption(item, "ClassLabels", istype="str", islist=True)
            if "ModelBinning" in item:
                self.__ModelBinning = hf.ReadOption(item, "ModelBinning", istype="float", islist=True)
            if "ClassColors" in item:
                self.__ClassColors = hf.ReadOption(item, "ClassColors", istype = "int", islist=True)

        #############################################################################
        ############################ Check Input Options ############################
        #############################################################################
        if self.__Name==None:
            ErrorMessage("Model with missing 'Name' attribute detected!.")
        if self.__Type==None:
            ErrorMessage("MODEL " + self.__Name + " has no 'Type' set.")
        elif self.__Type not in self.__AllowedTypes:
            ErrorMessage("MODEL " + self.__Name + " has unknown type " + self.__Type +". 'Type' can be 'Classification-DNN','Classification-BDT', 'Classification-WGAN' or'Z-Reconstruction'.")
        if self.__Epochs<=10:
            WarningMessage("MODEL " + self.__Name + " has 'Epochs' set to "+str(self.__Epochs)+". This is a very low number. The model might not converge.")
        if self.__ValidationSize>=1 or self.__ValidationSize<=0:
            ErrorMessage("MODEL " + self.__Name + " has 'ValidationSize' set to "+str(self.__ValidationSize)+". 'MinDelta' must be in (0,1).")
        if self.__ClassLabels != None: # If we are in a multi-class scenario we expect multiple custom binnings or have to set multiple defaults.
            if self.__ModelBinning == None:
                self.__ModelBinning = [[20,0,1] for nClassLabels in range(len(self.__ClassLabels))]
            elif len(self.__ClassLabels)*3!=len(self.__ModelBinning):
                ErrorMessage("MODEL " + self.__Name + " 'ModelBinning' must be list of values using this format: nbins, x_low, x_high. A binning must be defined for each multi-class classifier.")
            else:
                self.__ModelBinning = hf.chunks(self.__ModelBinning,3)
        else: # If we are in a binary scenario we expect one custom binning.
            if self.__ModelBinning == None:
                self.__ModelBinning = [[20,0,1]]
            elif len(self.__ModelBinning)!=3:
                ErrorMessage("MODEL " + self.__Name + " 'ModelBinning' must be list of values using this format: nbins, x_low, x_high.")

    ######################################################################
    ############################ Misc Methods ############################
    ######################################################################
    def smooth_labels(self, Y, K, alpha=0.1):
        '''
        Function to smooth labels as presented in arXiv:1906.02629 and arXiv:1512.00567

        Keyword Arguments:
        Y     --- Labels (list)
        alpha --- Smoothing parameter (should be between 0 and < 0.5)
        '''
        return Y*(1-alpha)+alpha/K

    def get_Name(self):
        return self.__Name

    def get_Type(self):
        return self.__Type

    def get_Epochs(self):
        return self.__Epochs

    def get_BatchSize(self):
        return self.__BatchSize

    def get_ValidationSize(self):
        return self.__ValidationSize

    def get_ClassLabels(self):
        return self.__ClassLabels

    def get_Metrics(self):
        return self.__Metrics

    def get_ModelBinning(self):
        return self.__ModelBinning

    def get_ClassColors(self):
        return self.__ClassColors

    def isClassification(self):
        return self.isClassificationDNN() or self.isClassificationBDT() or self.isClassificationWGAN()

    def isClassificationDNN(self):
        return self.__Type=="Classification-DNN"

    def isClassificationBDT(self):
        return self.__Type=="Classification-BDT"

    def isRegression(self):
        return self.__Type=="Regression"

    def isReconstruction(self):
        return self.__Type=="Z-Reconstruction" # add more reco types in here and below

    def isZReconstruction(self):
        return self.__Type=="Z-Reconstruction"

    def isClassificationWGAN(self):
        return self.__Type=="Classification-WGAN"

    ##############################################################################
    ############################ Printing Information ############################
    ##############################################################################

    def __str__(self):
        '''
        Method to represent the class objects as a very basic string.
        '''
        return ("MODELINFO:\tName: "+self.__Name
                +"\nMODELINFO:\tType: "+self.__Type
                +"\nMODELINFO:\tEpochs: "+str(self.__Epochs)
                +"\nMODELINFO:\tBatchSize: "+str(self.__BatchSize)
                +"\nMODELINFO:\tMetrics: "+str(self.__Metrics))
