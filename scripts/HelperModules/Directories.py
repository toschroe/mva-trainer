from HelperModules.HelperFunctions import create_output_dir, ensure_trailing_slash

class Directories:
    """This class defines output directories
    """
    def __init__(self, output_path):
        
        self.__MainDir      = output_path
        self.__PlotsDir     = ensure_trailing_slash(output_path)+"Plots"
        self.__MVAPlotsDir  = ensure_trailing_slash(self.__PlotsDir)+"MVAPlots"
        self.__CtrlPlotsDir = ensure_trailing_slash(self.__PlotsDir)+"CtrlPlots"
        self.__DataDir      = ensure_trailing_slash(output_path)+"Data"
        self.__ModelDir     = ensure_trailing_slash(output_path)+"Model"
        self.__ConfigDir    = ensure_trailing_slash(output_path)+"Configs"
        
        create_output_dir(self.__MainDir)      # This will be our main directory
        create_output_dir(self.__PlotsDir)     # This directory holds all plots
        create_output_dir(self.__CtrlPlotsDir) # This directory holds all control plots
        create_output_dir(self.__MVAPlotsDir)  # This directory holds all MVA plots
        create_output_dir(self.__DataDir)      # This directory holds converted data
        create_output_dir(self.__ModelDir)     # This directory holds the compiled models and their history
        create_output_dir(self.__ConfigDir)    # This directory holds copies of used config files

    def MainDir(self):
        return self.__MainDir
    
    def PlotDir(self):
        return self.__PlotsDir

    def CtrlPlotDir(self):
        return self.__CtrlPlotsDir

    def MVAPlotDir(self):
        return self.__MVAPlotsDir

    def DataDir(self):
        return self.__DataDir

    def ModelDir(self):
        return self.__ModelDir

    def ConfigDir(self):
        return self.__ConfigDir
