from HelperModules.MessageHandler import ErrorMessage, WarningMessage, TrainerMessage, TrainerDoneMessage

import pandas as pd
import json
import numpy as np
from numpy.random import random

import HelperModules.HelperFunctions as hf
from HelperModules.BasicModel import BasicModel
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from pickle import dump

class BDTModel(BasicModel):
    """
    Class for implementing boosted decision trees.
    """
    def __init__(self, group, input_dim, Variables, is_Train=True):
        super().__init__(group)
        self.__InputDim            = input_dim # Input dimensionality of the data
        self.__scikitmodel         = None      # Keras model object
        self.__PredesignedModel    = None      # For usage of pre-designed models
        self.__nEstimators         = 100       # Number of boosting stages to perform
        self.__MaxDepth            = 3         # Maximum depth of the individual estimators
        self.__LearningRate        = 0.1       # Learning rate
        self.__Patience            = None      # Number of epochs in ES to wait for improvement
        self.__MinDelta            = None      # Minimum improvement for ES
        self.__Criterion           = None      # Function to measure the quality of a split
        self.__Loss                = None      # Loss function to be used
        self.__SmoothingAlpha      = None      # Parameter to be used for label smoothing

        self.__AllowedBDTTypes     = ["Classification-BDT"]

        #######################################################################
        ############################ Input Options ############################
        #######################################################################
        for item in self._BasicModel__Group:
            if "nEstimators" in item:
                self.__nEstimators = hf.ReadOption(item, "nEstimators", istype="int", islist=False, msg="Could not load 'nEstimators'. Make sure you pass an integer greater than 0.")
            if "MaxDepth" in item:
                self.__MaxDepth = hf.ReadOption(item, "MaxDepth", istype="float", islist=False, msg="Could not load 'MaxDepth'. Make sure you pass an integer greater than 0.")
            if "LearningRate" in item:
                self.__LearningRate = hf.ReadOption(item, "LearningRate", istype="float", islist=False, msg="Could not load 'LearningRate'. Make sure you pass a float.")
            if "Patience" in item:
                self.__Patience = hf.ReadOption(item, "Patience", istype="int", islist=False, msg="Could not load 'Patience'. Make sure you pass an integer.")
            if "MinDelta" in item:
                self.__MinDelta = hf.ReadOption(item, "MinDelta", istype="float", islist=False, msg="Could not load 'MinDelta'. Make sure you pass a float.")
            if "Loss" in item:
                self.__Loss = hf.ReadOption(item, "Loss", istype="str", islist=False)
            if "SmoothingAlpha" in item:
                self.__SmoothingAlpha = hf.ReadOption(item, "SmoothingAlpha", istype="float", islist=False)

        #############################################################################
        ############################ Check Input Options ############################
        #############################################################################
        if self._BasicModel__Type not in self.__AllowedBDTTypes:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid type set for BDTMODEL!")
        if self.__MinDelta!=None:
            if self.__MinDelta>=1:
                ErrorMessage("MODEL " + self._BasicModel__Name + " has 'MinDelta' set to "+str(self.__MinDelta)+". 'MinDelta' must be <1.")
        if self.__Loss==None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has no 'Loss' set.")
        elif self.__Loss not in ["deviance", "exponential"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'Loss' set: "+self.__Loss)
        if self.__Criterion == None:
            self.__Criterion = "friedman_mse"
            if is_Train==True:
                WarningMessage("MODEL " + self._BasicModel__Name + " has no 'Criterion' set. Using 'friedman_mse' now.")
        if self.__Criterion not in ["friedman_mse", "mse"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unsupported'Criterion' set. Only 'friedman_mse' and 'mse' are supported.")
        if self._BasicModel__ClassLabels == None:
            self._BasicModel__ClassLabels = ["Binary"]
        if self.__SmoothingAlpha != None and (self.__SmoothingAlpha > 0.5 or self.__SmoothingAlpha<0):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid SmoothingAlpha value! Must be between >0 and <0.5!")

    def BDTCompile(self):
        self.__scikitmodel = GradientBoostingClassifier(n_estimators = self.__nEstimators,
                                                        learning_rate = self.__LearningRate,
                                                        loss = self.__Loss,
                                                        criterion  = self.__Criterion,
                                                        max_depth = self.__MaxDepth,
                                                        n_iter_no_change = self.__Patience,
                                                        validation_fraction = self._BasicModel__ValidationSize,
                                                        tol = self.__MinDelta,
                                                        verbose = 1)
        
    def CompileAndTrain(self, X, Y, W, output_path, Fold="", Variables=None):
        '''
        Training a simple GradientBoosting classifier.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model .joblib files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        '''
        if(Fold!=""):
            Fold = "_Fold"+Fold
        if self.__SmoothingAlpha!=None:
            Y=self.smooth_labels(Y, K=len(np.unique(y)), alpha=self.__SmoothingAlpha)
        self.BDTCompile()
        TrainerMessage("Starting Training...")
        self.__scikitmodel.fit(X, Y, sample_weight=W)
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".pkl", 'wb') as f:
            dump(self.__scikitmodel, f)
