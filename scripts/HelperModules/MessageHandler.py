import sys

class fontcolors:
    """This class defines fonts and colors for a nicer output on the terminal.

    Available Colors:
    HEADER, OKBLUE, OKCYAN, OKGREEN, WARNING, FAIL, ENDC, BOLD, UNDERLINE.
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def InfoDelimiter():
    print("##############################################")

def InfoMessage(text):
    print("INFO:\t" + text)

def InfoDoneMessage(text):
    print(fontcolors.OKGREEN + "CONVERTER:\t" + text + fontcolors.ENDC)

def PostProcessorMessage(text):
    print("POSTPROCESSOR:\t" + text)

def ConverterMessage(text):
    print("CONVERTER:\t" + text)

def ConverterDoneMessage(text):
    print(fontcolors.OKGREEN + "CONVERTER:\t" + text + fontcolors.ENDC)

def TrainerMessage(text):
    print("TRAINER:\t" + text)
    
def TrainerDoneMessage(text):
    print(fontcolors.OKGREEN + "TRAINER:\t" + text + fontcolors.ENDC)

def InjectorMessage(text):
    print("MVAINJECTOR:\t" + text)

def InjectorDoneMessage(text):
    print(fontcolors.OKGREEN + "MVAINJECTOR:\t" + text + fontcolors.ENDC)

def ErrorMessage(text, exit=True):
    print(fontcolors.FAIL + "ERROR:\t" + text +fontcolors.ENDC)
    if exit:
        print(fontcolors.FAIL + "ERROR:\tExiting!" +fontcolors.ENDC)
        sys.exit()

def WarningMessage(text):
    print(fontcolors.WARNING + "WARNING:\t" + text +fontcolors.ENDC)

def WelcomeMessage(option="Trainer"):
    if(option == "Trainer"):
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "########################## MVA - TRAINER ##########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif(option == "Converter"):
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Converter #########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif(option == "Evaluate"):
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Evaluater #########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif(option == "Injector"):
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Injector ##########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
