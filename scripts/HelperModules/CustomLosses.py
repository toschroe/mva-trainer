import keras.backend as K
import tensorflow as tf

def categorical_focal_loss(gamma=2.0, alpha=0.25):
    """
    Implementation of categorical Focal Loss based on arXiv:1708.02002 
    Formula:
        loss = -alpha*((1-p)^gamma)*log(p)

    Keyword Arguments:
    alpha --- Weighting factor similar to balanced cross entropy
    gamma --- Focusing parameter for modulating factor (1-p)
    Default values:
    gamma --- 2.0 as mentioned in the paper
    alpha --- 0.25 as mentioned in the paper
    """
    def focal_loss(y_true, y_pred):
        epsilon = K.epsilon() # Prevent NaN in backpropagation by adding epsilon to prediction value
        y_pred = K.clip(y_pred, epsilon, 1.0-epsilon)
        cross_entropy = -y_true*K.log(y_pred)
        weight = alpha * y_true * K.pow((1-y_pred), gamma) # Modulating factor and weighting factor
        loss = weight * cross_entropy
        loss = K.sum(loss, axis=1)
        return loss
    return focal_loss

def binary_focal_loss(gamma=2.0, alpha=0.25):
    """
    Implementation of binary Focal Loss based on arXiv:1708.02002
    Formula:
        loss = -alpha_t*((1-p_t)^gamma)*log(p_t)
        
        p_t = y_pred, if y_true = 1
        p_t = 1-y_pred, otherwise
        
        alpha_t = alpha, if y_true=1
        alpha_t = 1-alpha, otherwise
        
        cross_entropy = -log(p_t)
    Keyword Arguments:
    alpha --- Weighting factor similar to balanced cross entropy
    gamma --- Focusing parameter for modulating factor (1-p)
    Default values:
    gamma --- 2.0 as mentioned in the paper
    alpha --- 0.25 as mentioned in the paper
    """
    def focal_loss(y_true, y_pred):
        epsilon = K.epsilon()
        y_pred = K.clip(y_pred, epsilon, 1.0-epsilon)
        p_t = tf.where(K.equal(y_true, 1), y_pred, 1-y_pred)
        alpha_factor = K.ones_like(y_true)*alpha
        alpha_t = tf.where(K.equal(y_true, 1), alpha_factor, 1-alpha_factor)
        cross_entropy = -K.log(p_t)
        weight = alpha_t * K.pow((1-p_t), gamma)
        loss = weight * cross_entropy
        loss = K.sum(loss, axis=1)
        return loss
    return focal_loss

def wasserstein_loss(y_true, y_pred):
    """
    Wasserstein loss for a WGAN
    """
    return K.mean(y_true * y_pred)
