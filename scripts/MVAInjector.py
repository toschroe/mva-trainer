import argparse, keras, os, ROOT
import pandas as pd
import numpy as np
import tensorflow as tf
import itertools
import time

from pickle import load
from root_numpy import tree2array
from array import array
from multiprocessing import Pool
from HelperModules import Settings
from HelperModules.HelperFunctions import ensure_trailing_slash, create_output_dir
from HelperModules.MessageHandler import InjectorMessage, InjectorDoneMessage, ErrorMessage, WelcomeMessage

def inject(args, fname, ID, MAX_ID):
    # Here we create the necessary settings objects and read information from the config file
    InjectorMessage("Processing "+str(ID)+"/"+str(MAX_ID)+": "+fname)
    
    # Get meta information
    All_ModelObjects = [] # List of all models to be evaluated (in case we want to inject multiple)
    All_Scalers      = [] # List of all scalers to be used (in case we use multiple models)
    All_Names        = [] # List of all model names to be used (in case we use multiple models)
    All_Variables    = [] # List of variables to be used for each model respectively
    All_NFolds       = [] # List of number of folds per model
    All_ClassLabels  = [] # List of list of class labels for each model

    tf.compat.v1.disable_eager_execution()

    # Filling up the All_* variables with information taken from all model paths
    for configfile, model_path in zip(args.configfile,args.model_path):
        cfg_settings = Settings.Settings(configfile, option="all", is_Train=False)
        ClassLabels = cfg_settings.get_Model().get_ClassLabels()
        All_Variables.append(cfg_settings.get_General().get_Variables())
        nFolds = cfg_settings.get_General().get_Folds()
        Model = cfg_settings.get_Model()
        All_NFolds.append(nFolds)
        if(nFolds!=1):
            Folds = ["_Fold"+str(i) for i in range(nFolds)]
        else:
            Folds = [""]
        if Model.isClassificationBDT():
            ModelNames = [ensure_trailing_slash(model_path)+cfg_settings.get_Model().get_Name()+"_"+cfg_settings.get_Model().get_Type()+Fold+".pkl" for Fold in Folds]
            ModelObjects = []
            for modelname in ModelNames:
                with open(modelname, 'rb') as f:
                    ModelObjects.append(load(f))
        if Model.isClassificationDNN():
            ModelNames = [ensure_trailing_slash(model_path)+cfg_settings.get_Model().get_Name()+"_"+cfg_settings.get_Model().get_Type()+Fold+".h5" for Fold in Folds]
            ModelObjects = [keras.models.load_model(modelname) for modelname in ModelNames]
        
        scaler = load(open(ensure_trailing_slash(model_path)+"scaler.pkl", 'rb'))
        All_Names.append(cfg_settings.get_Model().get_Name())
        All_ClassLabels.append(ClassLabels)
        All_ModelObjects.append(ModelObjects)
        All_Scalers.append(scaler)

    # We need a dictionary holding the model names and corresponding class labels to distinguish binary from multi-class cases
    Name_dict = dict(zip(All_Names,All_ClassLabels))

    # We also need another dictionary holding the names and the coresponding prediction dictionary which holds the relation eventNumber:Prediction
    Name_Prediction_dict = {}

    # Create a new root file
    File = ROOT.TFile(fname)
    NewFile = ROOT.TFile(fname.replace(os.path.abspath(args.input_path), os.path.abspath(args.output_path)),"recreate")

    # Loop over trees in root file
    keys = File.GetListOfKeys()
    Treenames = []
    for key in keys:
        if(key.GetClassName() != "TTree"):
            continue
        else:
            Treenames.append(key.ReadObj().GetName())
    for tName in Treenames:
        Tree = File.Get(tName)
        if tName not in args.tree_wildcard and args.tree_wildcard != "":
            continue
        if tName in ["", "truth", "particleLevel", "PDFsumWeights", "AnalysisTracking"]: # black listing a few trees which are simply copied
            NewTree = Tree.CloneTree()
            NewFile.Write()
            continue
        else:
            NewTree = Tree.CloneTree(0)

        # We create an array and a corresponding branch for every model (and class for multi-class) that is evaluated to hold the prediction
        New_Branches = {}
        for name, classlabels in Name_dict.items():
            if(len(classlabels)==1):
                # Binary classifier
                New_Branches[name] = array('f', [0])
            else:
                # For multi-class classifiers we add separate branches for each classifier
                for label in classlabels:
                    New_Branches[name+"_"+label] = array('f', [0])
        for branch_name, new_branch in New_Branches.items():
            NewTree.Branch(branch_name, new_branch, branch_name+"/F")

        if Tree.GetEntries() == 0:
            NewFile.Write()
            continue
        # Here we pre-calculate the predictions because is faster to calculate them all at once -> We only do one matrix multiplication once
        Prediction_Array = []
        Name_Array = []
        for name,Variables,nFolds,scaler,ModelObjects in zip(All_Names,All_Variables,All_NFolds,All_Scalers,All_ModelObjects):
            Vars = Variables.copy()
            Vars+=["eventNumber"]
            X = np.array(tree2array(Tree, branches=Vars))
            df_X = pd.DataFrame(data = np.array(X), columns = Vars)
            EventNumbers = df_X["eventNumber"].values
            df_X_Folds = [df_X[df_X["eventNumber"]%nFolds==i] for i in range(nFolds)]
            # Let's reorder the event numbers and concatenate them again. We need this for the keys of our prediction dictionary
            EventNumbers = [df_X[df_X["eventNumber"]%nFolds==i]["eventNumber"].values for i in range(nFolds)]
            EventNumbers = np.array(list(itertools.chain.from_iterable(EventNumbers)))
            df_X_Folds = [X.drop(["eventNumber"], axis=1) for X in df_X_Folds]

            # We need to make sure we actually have values to pass to the scaler. Otherwise this will crash
            X_Scaled = [scaler.transform(X.values) if (len(X.values)!=0) else [] for X in df_X_Folds]
            if Model.isClassificationBDT():
                if len(Name_dict[name])==1: # binary scikit-learn case returns [1-P,P]
                    Model_Predictions = [ModelObjects[i].predict_proba(X)[:,1] if len(X)!=0 else [] for i,X in zip(range(nFolds),X_Scaled)]
                else:
                    Model_Predictions = [ModelObjects[i].predict_proba(X) if len(X)!=0 else [] for i,X in zip(range(nFolds),X_Scaled)]
            if Model.isClassificationDNN():
                Model_Predictions = [ModelObjects[i].predict(X) if len(X)!=0 else [] for i,X in zip(range(nFolds),X_Scaled)]
            if len(Name_dict[name])==1:
                # binary classifier
                Prediction_Array.append(list(itertools.chain.from_iterable(Model_Predictions)))
                Name_Array.append(name)
            else:
                # for multi-class classifiers we add separate branches for each classifier and make sure we add the correct column (class_ID)
                for class_ID,label in enumerate(Name_dict[name]):
                    Prediction_Array.append(np.array(list(itertools.chain.from_iterable(Model_Predictions)))[:,class_ID])
                    Name_Array.append(name+"_"+label)

        # Now we iterate through our prediction array and use the name as a key to save into the correct branch.
        Prediction_Array = np.array(Prediction_Array)
        if len(Name_Array)==1:
            Prediction_dicts  = [dict(zip(Name_Array,Values)) for Values in Prediction_Array.T[0]]
        else:
            Prediction_dicts  = [dict(zip(Name_Array,Values)) for Values in Prediction_Array.T]
        EventNumber_Lookup_Dict = dict(zip(EventNumbers,Prediction_dicts))
        for entry_number in range(0,Tree.GetEntries()):
            Tree.GetEntry(entry_number)
            for Name in Name_Array:
                New_Branches[Name][0] = EventNumber_Lookup_Dict[Tree.eventNumber][Name]
            NewTree.Fill()
        NewFile.Write()
    NewFile.Close()
    InjectorDoneMessage("Finished "+fname)

def absoluteFilePaths(directory):
    for dirpath,_,filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--output_path", help="output path for the converted ntuples", required=True)
    parser.add_argument("-c", "--configfile", help="Config file", nargs='+', required=True)
    parser.add_argument("-i", "--input_path", help="Ntuple path", required=True)
    parser.add_argument("-f", "--filter", dest="wildcard", help="additional string to filter input files", type=str, default='')
    parser.add_argument("-t", "--treefilter", dest="tree_wildcard", help="additional string to filter trees", type=str, default='')
    parser.add_argument("-m", "--model-path", help="input path of the ML model and history", nargs='+', required=True)
    parser.add_argument("--processes", help="the number of parallel processes to run", type=int, default=8)
    args = parser.parse_args()

    WelcomeMessage("Injector")

    #Ignoring tf warnings and info messages
    tf.get_logger().setLevel('ERROR')
    
    if os.environ.get("TTZ_NTUPLE_LOCATION") is None:
        ErrorMessage("TTZ_NTUPLE_LOCATION undefined. Did you forget to source the setup script?")
    if not os.path.isdir(os.environ["TTZ_NTUPLE_LOCATION"]+os.environ["TTZ_NTUPLE_VERSION"]+"/"):
        ErrorMessage(os.environ["TTZ_NTUPLE_LOCATION"]+os.environ["TTZ_NTUPLE_VERSION"]+"/ does not seem to exist. You need to check this!")

    # Let's make sure the output directory exists. This is done recursively and should work for python>3.5
    for dirpath, dirnames, filenames in os.walk(args.input_path):
        structure = os.path.join(args.output_path, dirpath[len(args.input_path):].strip("/"))
        if not os.path.isdir(structure):
            os.makedirs(structure)
    fnames = absoluteFilePaths(args.input_path)
    fnames = [f for f in fnames if args.wildcard in f or args.wildcard=='']
    p = Pool(args.processes, maxtasksperchild=1)
    MAX_ID = len(fnames)
    fnames = [p.apply_async(inject, (args, fname, ID+1, MAX_ID,)) for ID,fname in enumerate(fnames)]
    p.close()
    p.join()
